module.exports = {
    title: '基础创新',
    description: '杭州基础创新科技有限公司',
    themeConfig: {
        nav: [
          { text: '指南', link: '/guide/' },
          { text: '基础应用', link: '/basic/' },
          { text: '高级应用', link: '/advance/' },
          { text: '运行维护', link: '/weihu/' },
          { text: '统计分析', link: '/fenxi/' },
          { text: '系统管理', link: '/sys/' },
          { text: '计费应用', link: '/jifei/' },
          { text: '了解更多',
                items: [
                    { text: '列表一', link: '/language/chinese/' },
                    { text: '列表二', link: '/language/japanese/' },
                    { text: '列表三', link: '/language/japanese/' },
                    { text: '列表四', link: '/language/japanese/' }
                ] }
        ],
        sidebar: {
            '/guide/': [
                       "",
                       "first", 
                       "second"
                         ],
            '/basic/': [
                        "",
                        "data_design", 
                        "test1", 
                        "test2"
                         ],
          "/advance/": [
                        "",
                        "test1", 
                        "test2"
                         ]
             }
      }
}
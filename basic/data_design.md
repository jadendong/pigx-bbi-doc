# 数据库设计

## 一、首页统计方面表设计

+ [设备状态信息表 ](#1)
+ [日消费汇总表 ](#2.1)
+ [月消费汇总表](#2.2)
+ [年消费汇总表](#2.3)
+ [日缴费金额表](#3.1)
+ [月缴费金额表](#3.2)
+ [年缴费金额表](#3.3)
+ [日用量表](#4.1)
+ [月用量表](#4.2)
+ [年用量表](#4.3)
+ [楼宇当前欠费表](#5)
+ [消息列表表](#6)
+ [消息类型表](#7)
+ [用户日消费汇总表](#8.1)
+ [用户月消费汇总表](#8.2)
+ [用户年消费汇总表](#8.3)
+ [用户日缴费金额表](#9.1)
+ [用户月缴费金额表](#9.2)
+ [用户年缴费金额表](#9.3)
+ [用户日用量表](#10.1)
+ [用户月用量表](#10.2)
+ [用户年用量表](#10.3)

###  <a id="1"> 1.  `stat_sbzt` 设备状态信息表</a>

| 字段名  | 类型               | 备注                             |
| ------- | ------------------ | -------------------------------- |
| `yhzs`  | `integer unsigned` | 用户总数                         |
| `yhkh`  | `integer unsigned` | 预付费开户总数                   |
| `yhwkh` | `integer unsigned` | 预付费未开户总数                 |
| `dbzs`  | `integer unsigned` | 电表总数                         |
| `dbzx`  | `integer unsigned` | 电表在线数                       |
| `dblx`  | `integer unsigned` | 电表离线数                       |
| `sbzs`  | `integer unsigned` | 水表总数                         |
| `sbzx`  | `integer unsigned` | 水表在线数                       |
| `sblx`  | `integer unsigned` | 水表离线数                       |
| `ktzs`  | `integer unsigned` | 空调总数                         |
| `ktzx`  | `integer unsigned` | 空调在线数                       |
| `ktlx`  | `integer unsigned` | 空调离线数                       |
| `tjsj`  | `datetime`         | 统计的时间（按天统计）`yy-MM-dd` |
| `dwdm`  | `varchar(20)`      | 单位代码                         |
| `xmid`  | `varchar(20)`      | 项目ID                           |



###  <a id="2.1"> 2.1 `stat_xfhz_r` 日消费汇总表</a>

| 字段名  | 类型           | 备注           |
| ------- | -------------- | -------------- |
| `xfe`   | `decimal`      | 消费额         |
| `sxfe`  | `decimal`      | 水消费总额     |
| `dxfe`  | `decimal`      | 电消费总额     |
| `ktxfe` | `decimal`      | 空调消费总额   |
| `tjsj` | `datetime`     | 统计时间`yyyy-MM-dd` |
| `dwdm`  | `varchar(20)`  | 单位代码       |
| `xmid`  | `varchar(20)` | 项目ID         |

### <a id="2.2"> 2.2 `stat_xfhz_y` 月消费汇总表 </a>

| 字段名  | 类型           | 备注           |
| ------- | -------------- | -------------- |
| `xfe`   | `decimal`      | 消费额         |
| `sxfe`  | `decimal`      | 水消费总额     |
| `dxfe`  | `decimal`      | 电消费总额     |
| `ktxfe` | `decimal`      | 空调消费总额   |
| `tjsj` | `datetime`     | 统计时间`yyyy-MM` |
| `dwdm`  | `varchar(20)`  | 单位代码       |
| `xmid`  | `varchar(20)` | 项目ID         |

###  <a id="2.3">2.3 `stat_xfhz_n` 年消费汇总表</a>

| 字段名  | 类型           | 备注           |
| ------- | -------------- | -------------- |
| `xfe`   | `decimal`      | 消费额         |
| `sxfe`  | `decimal`      | 水消费总额     |
| `dxfe`  | `decimal`      | 电消费总额     |
| `ktxfe` | `decimal`      | 空调消费总额   |
| `tjsj` | `datetime`     | 统计时间`yyyy` |
| `dwdm`  | `varchar(20)`  | 单位代码       |
| `xmid`  | `varchar(20)` | 项目ID         |




### <a id="3.1"> 3.1 `stat_jf_r` 日缴费金额表 </a>

| 字段名 | 类型           | 备注     |
| ------ | -------------- | -------- |
| `jfe`  | `decimal`      | 缴费金额 |
| `tjsj` | `datetime`     | 统计时间`yyyy-MM-dd` |
| `dwdm` | `varchar(20)`  | 单位代码 |
| `xmid` | `varchar(20)` | 项目ID   |

### <a id="3.2">  3.2 `stat_jf_y` 月缴费金额表 </a>

| 字段名 | 类型           | 备注     |
| ------ | -------------- | -------- |
| `jfe`  | `decimal`      | 缴费金额 |
| `tjsj` | `datetime`     | 统计时间`yyyy-MM` |
| `dwdm` | `varchar(20)`  | 单位代码 |
| `xmid` | `varchar(20)` | 项目ID   |

###  <a id="3.3"> 3.3 `stat_jf_n` 年缴费金额表 </a>

| 字段名 | 类型           | 备注     |
| ------ | -------------- | -------- |
| `jfe`  | `decimal`      | 缴费金额 |
| `tjsj` | `datetime`     | 统计时间`yyyy` |
| `dwdm` | `varchar(20)`  | 单位代码 |
| `xmid` | `varchar(20)` | 项目ID   |



### <a id="4.1"> 4.1 `stat_yl_r` 日用量表 </a>

| 字段名 | 类型           | 备注          |
| ------ | -------------- | ------------- |
| `ydl`  | `decimal`      | 用电量        |
| `ysl`  | `decimal`      | 用水量        |
| `kt`   | `decimal`      | 空调          |
| `tjsj`   | `datetime`     | 统计`yyyy-MM-dd` |
| `dwdm` | `varchar(20)`  | 单位代码      |
| `xmid` | `varchar(20)` | 项目ID        |


### <a id="4.2"> 4.2 `stat_yl_y` 月用量表 </a>

| 字段名 | 类型           | 备注          |
| ------ | -------------- | ------------- |
| `ydl`  | `decimal`      | 用电量        |
| `ysl`  | `decimal`      | 用水量        |
| `kt`   | `decimal`      | 空调          |
| `tjsj`   | `datetime`     | 统计时间`yyyy-MM` |
| `dwdm` | `varchar(20)`  | 单位代码      |
| `xmid` | `varchar(20)` | 项目ID        |

### <a id=4.3> 4.3 `stat_yl_n` 年用量表 </a>

| 字段名 | 类型           | 备注          |
| ------ | -------------- | ------------- |
| `ydl`  | `decimal`      | 用电量        |
| `ysl`  | `decimal`      | 用水量        |
| `kt`   | `decimal`      | 空调          |
| `tjsj`   | `datetime`     | 统计时间`yyyy-MM` |
| `dwdm` | `varchar(20)`  | 单位代码      |
| `xmid` | `varchar(20)` | 项目ID        |



### <a id ="5">  5. `stat_lyqf` 楼宇当前欠费表 </a>

| 字段名   | 类型               | 备注           |
| -------- | ------------------ | -------------- |
| `qfhs`   | `integer unsigned` | 欠费户数       |
| `jjqfhs` | `integer unsigned` | 即将欠费户数   |
| `lzzs`   | `integer unsigned` | 拉闸总数       |
| `tjsj`   | `datetime`         | 统计时间（天） |
| `lyid`   | `varchar(20)`     | 楼宇ID         |
| `dwdm`   | `varchar(20)`      | 单位代码       |
| `xmid`   | `varchar(20)`     | 项目ID         |


### <a id="6">  6. `stat_xxlb` 消息列表表 </a>

| 字段名   | 类型                 | 备注                           |
| -------- | -------------------- | ------------------------------ |
| `xxnr`   | `varchar(255)`       | 消息内容                       |
| `xxlxid` | ``integer unsigned`` | 消息类型ID (外键消息类型)      |
| `sfyd`   | `char(1)`            | 是否已读`y/n`                  |
| `sfjx`   | `char(1)`            | 是否加星`y/n`                  |
| `xxsj`   | `datetime`           | 消息时间`yyyy-MM-dd hh:mm:ss ` |
| `dwdm`   | `varchar(20)`        | 单位代码                       |
| `xmid`   | `varchar(20)`       | 项目ID                        |



### <a id="7"> 7. `stat_xxlx` 消息类型表 </a>

| 字段名   | 类型               | 备注       |
| -------- | ------------------ | ---------- |
| `xxlxid` | `integer unsigned` | 消息类型ID |
| `xxlx`   | `varchar(255)`     | 消息类型   |

---

<u>以下是以用户为角度的</u>

---



### <a id="8.1">   8.1 `stat_yhxf_r` 用户日消费汇总表 </a>

| 字段名  | 类型           | 备注          |
| ------- | -------------- | ------------- |
| `xfy`   | `decimal`      | 消费额        |
| `dxfe`  | `decimal`      | 电消费额      |
| `sxfe`  | `decimal`      | 水消费额      |
| `ktxfe` | `decimal`      | 空调消费额    |
| `tjsj`    | `datetime`     | 月份`yyyy-MM-dd` |
| `yhid`  | `varchar(32)`  | 用户ID        |
| `dwdm`  | `varchar(20)`  | 单位代码      |
| `xmid`  | `varchar(20)` | 项目ID        |

### <a id="8.2">   8.2 `stat_yhxf_y` 用户月消费汇总表 </a>

| 字段名  | 类型           | 备注          |
| ------- | -------------- | ------------- |
| `xfy`   | `decimal`      | 消费额        |
| `dxfe`  | `decimal`      | 电消费额      |
| `sxfe`  | `decimal`      | 水消费额      |
| `ktxfe` | `decimal`      | 空调消费额    |
| `tjsj`    | `datetime`     | 月份`yyyy-MM` |
| `yhid`  | `varchar(32)`  | 用户ID        |
| `dwdm`  | `varchar(20)`  | 单位代码      |
| `xmid`  | `varchar(20)` | 项目ID        |

### <a id="8.3">  8.3 `stat_yhxf_n` 用户年消费汇总表 </a>

| 字段名  | 类型           | 备注          |
| ------- | -------------- | ------------- |
| `xfy`   | `decimal`      | 消费额        |
| `dxfe`  | `decimal`      | 电消费额      |
| `sxfe`  | `decimal`      | 水消费额      |
| `ktxfe` | `decimal`      | 空调消费额    |
| `tjsj`    | `datetime`     | 统计时间`yyyy` |
| `yhid`  | `varchar(32)`  | 用户ID        |
| `dwdm`  | `varchar(20)`  | 单位代码      |
| `xmid`  | `varchar(20)` | 项目ID        |



### <a id="9.1"> 9.1 `stat_yhjf_r` 用户日缴费金额表 </a>

| 字段名 | 类型           | 备注     |
| ------ | -------------- | -------- |
| `jfe`  | `decimal`      | 缴费金额 |
| `tjsj` | `datetime`     | 统计时间`yyyy-MM-dd` |
| `yhid` | `varchar(32)` | 用户ID |
| `dwdm` | `varchar(20)`  | 单位代码 |
| `xmid` | `varchar(20)` | 项目ID   |

### <a id="9.2"> 9.2 `stat_yhjf_y` 用户月缴费金额表 </a>

| 字段名 | 类型           | 备注     |
| ------ | -------------- | -------- |
| `jfe`  | `decimal`      | 缴费金额 |
| `tjsj` | `datetime`     | 统计时间`yyyy-MM` |
| `yhid` | `varchar(32)` | 用户ID |
| `dwdm` | `varchar(20)`  | 单位代码 |
| `xmid` | `varchar(20)` | 项目ID   |

### <a id="9.3"> 9.3 `stat_yhjf_n` 用户年缴费金额表 </a>

| 字段名 | 类型           | 备注     |
| ------ | -------------- | -------- |
| `jfe`  | `decimal`      | 缴费金额 |
| `tjsj` | `datetime`     | 统计时间`yyyy` |
| `yhid` | `varchar(32)` | 用户ID |
| `dwdm` | `varchar(20)`  | 单位代码 |
| `xmid` | `varchar(20)` | 项目ID   |



### <a id="10.1">  10.1 `stat_yhyl_r` 用户日用量表 </a>

| 字段名 | 类型           | 备注          |
| ------ | -------------- | ------------- |
| `ydl`  | `decimal`      | 用电量        |
| `ysl`  | `decimal`      | 用水量        |
| `kt`   | `decimal`      | 空调          |
| `tjsj`   | `datetime`     | 统计`yyyy-MM-dd` |
| `yhid` | `varchar(32)` | 用户ID |
| `dwdm` | `varchar(20)`  | 单位代码      |
| `xmid` | `varchar(20)` | 项目ID        |


### <a id="10.2"> 10.2 `stat_yhyl_y` 用户月用量表 </a>

| 字段名 | 类型           | 备注          |
| ------ | -------------- | ------------- |
| `ydl`  | `decimal`      | 用电量        |
| `ysl`  | `decimal`      | 用水量        |
| `kt`   | `decimal`      | 空调          |
| `tjsj`   | `datetime`     | 统计时间`yyyy-MM` |
| `yhid` | `varchar(32)` | 用户ID |
| `dwdm` | `varchar(20)`  | 单位代码      |
| `xmid` | `varchar(20)` | 项目ID        |

### <a id="10.3"> 10.3 `stat_yhyl_n` 用户年用量表 </a>

| 字段名 | 类型           | 备注          |
| ------ | -------------- | ------------- |
| `ydl`  | `decimal`      | 用电量        |
| `ysl`  | `decimal`      | 用水量        |
| `kt`   | `decimal`      | 空调          |
| `tjsj`   | `datetime`     | 统计时间`yyyy-MM` |
| `yhid` | `varchar(32)` | 用户ID |
| `dwdm` | `varchar(20)`  | 单位代码      |
| `xmid` | `varchar(20)` | 项目ID        |



## 二、财务报表



###  日充值记录统计

| 字段名 | 类型          | 备注       |
| ------ | ------------- | ---------- |
| `zje`  | `decimal`     | 总金额     |
| `tjsj` | `datetime`    | 时间（天） |
| `xmid` | `varchar(20)` | 项目ID     |
| `dwdm` | `varchar(20)` | 单位代码   |



### 月充值记录统计

| 字段名 | 类型          | 备注       |
| ------ | ------------- | ---------- |
| `zje`  | `decimal`     | 总金额     |
| `tjsj` | `datetime`    | 时间（月） |
| `xmid` | `varchar(20)` | 项目ID     |
| `dwdm` | `varchar(20)` | 单位代码   |



### 月总表金额表

| 字段名 | 类型          | 备注       |
| ------ | ------------- | ---------- |
| `zbje` | `decimal`     | 总表金额   |
| `tjsj` | `datetime`    | 时间（月） |
| `xmid` | `varchar(20)` | 项目ID     |
| `dwdm` | `varchar(20)` | 单位代码   |



### 日撤销订单笔数

| 字段名  | 类型          | 备注       |
| ------- | ------------- | ---------- |
| 字段名  | 类型          | 备注       |
| `cxbs`  | `decimal`     | 撤销笔数   |
| `cxzje` | `decimal`     | 撤销总金额 |
| `xmid`  | `varchar(20)` | 项目ID     |
| `dwdm`  | `varchar(20)` | 单位代码   |



###  收支表

| 字段名 | 类型          | 备注       |
| ------ | ------------- | ---------- |
| `zsr`  | `decimal`     | 总收入     |
| `zzc`  | `deciaml`     | 总支出     |
| `tjsj` | `decimal`     | 时间(月份) |
| `xmid` | `varcahr(20)` | 项目ID     |
| `dwdm` | `varchar(20)` | 单位代码   |



### 天成交笔数

| 字段名 | 类型          | 备注       |
| ------ | ------------- | ---------- |
| `cjbs` | `int`         | 成交笔数   |
| `cje`  | `cje`         | 成交额     |
| `tjsj` | `datetime`    | 时间（天） |
| `xmid` | `varchar(20)` | 项目ID     |
| `dwdm` | `varchar(20)` | 单位代码   |



### 月成交笔数表

| 字段名 | 类型 | 备注     |
| ------ | ---- | -------- |
| `cjbs` | `int` | 成交笔数 |
| `cje` | `decimal` | 成交额   |
| `tjsj` | `datetime` | 时间（月）   |
| `xmid` | `varchar(20)` | 项目ID |
| `dwdm` | `varcahr(20)` | 单位代码  |



### 用户充值记录表

| 字段名 | 类型           | 备注     |
| :----- | :------------- | :------- |
| `yhid` | `varchar(32)`  | 用户ID   |
| `yhxm` | `varchar(255)` | 姓名     |
| `dz`   | `varchar(255)` | 地址     |
| `je`   | `decimal`      | 金额     |
| `czsj` | `datetime`     | 充值时间 |
| `xmid` | `varchar(20)`  | 项目ID   |
| `dwdm` | `varchar(20)`  | 单位代码 |



### 财务报表

| 字段名 | 类型           | 备注     |
| ------ | -------------- | -------- |
| `shmc` | `varchar(255)` | 商户名称 |
| `hh`   | `varchar(32)`  | 户号     |
| `zdsj` | `datetime`     | 账单时间 |
| `sqds` | `decimal`      | 上期读数 |
| `beqd` | `decimal`      | 本期读数 |
| `syds` | `decimal`      | 使用读数 |
| `dj`   | `decimal`      | 单价     |
| `czje` | `decimal`      | 充值金额 |
| `xmid` | `varchar(20)`  | 项目ID   |
| `dwdm` | `varchar(20)`  | 单位代码 |



## 三、

充值记录表

用户账单表

消费账单表

